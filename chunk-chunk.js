(function(PIXI) {
    var PLANE_BLOCK_SIZE = 40;
    var Plane = function (planeCont) {
        this._planeCont = planeCont;
        this._blocks = [];
        this._queue = [];
        var th = this;
        setInterval(function () {
            for(var i = 0; i < 5; i ++)
                if(th._queue.length > 0)
                    th._queue.pop()();
        }, 150);
        for(var i = 0; i < 5; i ++) {
            setTimeout(function () {
                setInterval(function () {
                    for(var i = 0; i < 5; i ++)
                        if(th._queue.length > 0)
                            th._queue.splice(Math.floor(Math.random()*th._queue.length), 1)[0]();
                }, 150);
            }, i * 80);
        }
    };
    Plane.prototype._getBlock = function (xi, yi, instancely) {
        var th = this;
        if(!this._blocks[yi])
            this._blocks[yi] = [];
        if(!this._blocks[yi][xi])
            if(instancely) {
                th._blocks[yi][xi] = th._createBlock(xi, yi);
            } else {
                this._queue.push(function() {
                    th._blocks[yi][xi] = th._createBlock(xi, yi);
                });
            }
        return this._blocks[yi][xi];
    };
    Plane.prototype._fillBlock = function (block) {
        block.rect.clear();
        block.rect.lineStyle(0, 0x000000);
        block.rect.beginFill(0x000000);
        block.rect.drawRect(0, 0, PLANE_BLOCK_SIZE - 4, PLANE_BLOCK_SIZE - 4);
        block.rect.endFill();
        block.filled = true;
    };
    Plane.prototype._unfillBlock = function (block) {
        block.rect.clear()
        block.rect.lineStyle(1, 0x000000);
        block.rect.drawRect(0, 0, PLANE_BLOCK_SIZE - 4, PLANE_BLOCK_SIZE - 4);
        block.rect.updateLocalBounds();
        block.filled = false;
    };
    Plane.prototype._checkFill = function (xi, yi) {
        var block = this._getBlock(xi, yi);
        if (!block)
            return false;
        return block.filled;
    }
    Plane.prototype._clickBlock = function (xi, yi) {
        var block = this._getBlock(xi, yi, true);
        if (block.filled) {
            this._flipFill(this._getBlock(xi - 1, yi, true));
            this._flipFill(this._getBlock(xi + 1, yi, true));
            this._flipFill(this._getBlock(xi, yi - 1, true));
            this._flipFill(this._getBlock(xi, yi + 1, true));
            this._unfillBlock(block);
        } else if (!this._checkFill(xi-1, yi) && !this._checkFill(xi+1, yi) && !this._checkFill(xi, yi-1) &&
                    !this._checkFill(xi, yi+1)) {
            this._fillBlock(this._getBlock(xi, yi, true));
        }
    };
    Plane.prototype._flipFill = function (block) {
        if(block.filled)
            this._unfillBlock(block);
        else
            this._fillBlock(block);
    }
    Plane.prototype.generate = function(x, y, w, h) {
        for(var yi = Math.floor(y / PLANE_BLOCK_SIZE) + 2; (yi+2) * PLANE_BLOCK_SIZE < y + h; yi ++) {
            for(var xi = Math.floor(x / PLANE_BLOCK_SIZE) + 2; (xi+2) * PLANE_BLOCK_SIZE < x + w; xi ++) {
                this._getBlock(xi, yi);
            }
        }
    };
    Plane.prototype._createBlock = function (xi, yi) {
        if(!this._blocks[yi])
            this._blocks[yi] = [];
        if(this._blocks[yi][xi])
            return this._blocks[yi][xi];
        var block = {
            rect: new PIXI.Graphics(),
            filled: false
        };
        block.rect.x = xi * PLANE_BLOCK_SIZE + 2,
        block.rect.y = yi * PLANE_BLOCK_SIZE + 2;
        this._blocks[yi][xi] = block;
        this._planeCont.addChild(block.rect);
        var th = this;
        var clickArea = new PIXI.Sprite(PIXI.Texture.EMPTY);
        clickArea.x = block.rect.x;
        clickArea.y = block.rect.y;
        clickArea.width = clickArea.height = PLANE_BLOCK_SIZE - 4;
        clickArea.interactive = true;
        clickArea.click = clickArea.tap = function () {
            th._clickBlock(xi, yi);
        };
        this._planeCont.addChild(clickArea);
        this._unfillBlock(block);
        return block;
    };
    var ChunkChunk = function () {
        this._stage = new PIXI.Stage(0xeeeeee, true);
        this._renderer = PIXI.autoDetectRenderer(200, 200);
        this._renderer.backgroundColor = 0xeeeeee;
        this._canvas = this._renderer.view;
        this._planeCont = new PIXI.Container();
        this._stage.addChild(this._planeCont);
        this._planeCont.x = this._planeCont.y = 400;
        this._plane = new Plane(this._planeCont);
        this._plaX = this._plaY = 0;
        var th = this;
        setInterval(function () {
            var xdiff = th._plaX - th._planeCont.x;
            var ydiff = th._plaY - th._planeCont.y;
            if(Math.abs(xdiff) <= 3) {
                th._planeCont.x = th._plaX;
            } else {
                th._planeCont.x += xdiff / 20;
            }
            if(Math.abs(ydiff) <= 3) {
                th._planeCont.y = th._plaY;
            } else {
                th._planeCont.y += ydiff / 20;
            }
        }, 10);

        this.setSize(200, 200);
    };
    /**
     * ChunkChunk::appendTo ( Node ) or ( jQuery )
     */
    ChunkChunk.prototype.appendTo = function (parent) {
        if (parent.append)
            parent.append(this._canvas);
        else if (parent.appendChild)
            parent.appendChild(this._canvas)
        else
            throw new TypeError("Expected element either DOM or jQuery.");
    };
    ChunkChunk.prototype.setSize = function (width, height) {
        this._width = width;
        this._height = height;
        this._renderer.resize(width, height);
        this._plane.generate(-this._plaX, -this._plaY, width, height);
    };
    ChunkChunk.prototype.redraw = function () {
        this._renderer.render(this._stage);
    };
    ChunkChunk.prototype.move = function (revx, revy) {
        var nx = this._plaX - revx;
        var ny = this._plaY - revy;
        this._plaX = nx;
        this._plaY = ny;
        this._plane.generate(-nx, -ny, this._width, this._height);
    };

    window.addEventListener("load", function () {
        function closeDig () {
            var el = document.getElementsByClassName('floatdesc')[0];
            if(el) {
                el.remove();
            }
        }
        var cc = new ChunkChunk();
        cc.appendTo(document.body);
        cc.setSize(window.innerWidth, window.innerHeight);
        window.addEventListener("resize", function () {
            cc.setSize(window.innerWidth, window.innerHeight);
        });
        window.addEventListener('keydown', function (evt) {
            switch(evt.keyCode) {
                case 37:
                case 72:
                    cc.move(-70, 0);
                    break;
                case 38:
                case 75:
                    cc.move(0, -70);
                    break;
                case 39:
                case 76:
                    cc.move(70, 0);
                    break;
                case 40:
                case 74:
                    cc.move(0, 70);
            }
            closeDig();
        });
        function draw() {
            requestAnimationFrame(draw);
            cc.redraw();
        }
        draw();
        document.getElementsByTagName('canvas')[0].addEventListener("mousedown", closeDig);
    });
})(PIXI);
